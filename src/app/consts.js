export const formIoData = {
  display: "form",
  components: [
    {
      label: "Nom",
      tableView: true,
      key: "nom",
      type: "textfield",
      input: true,
    },
    {
      label: "Prenom",
      tableView: true,
      key: "prenom",
      type: "textfield",
      input: true,
    },
    {
      label: "Date de naissance",
      tableView: false,
      datePicker: {
        disableWeekends: false,
        disableWeekdays: false,
      },
      enableMinDateInput: false,
      enableMaxDateInput: false,
      key: "dateDeNaissance",
      type: "datetime",
      input: true,
      widget: {
        type: "calendar",
        displayInTimezone: "viewer",
        locale: "en",
        useLocaleSettings: false,
        allowInput: true,
        mode: "single",
        enableTime: true,
        noCalendar: false,
        format: "yyyy-MM-dd hh:mm a",
        hourIncrement: 1,
        minuteIncrement: 1,
        time_24hr: false,
        minDate: null,
        disableWeekends: false,
        disableWeekdays: false,
        maxDate: null,
      },
    },
    {
      label: "Numero de telephone",
      inputMask: "(999) 99-99-999-999",
      tableView: true,
      key: "numeroDeTelephone",
      type: "phoneNumber",
      input: true,
    },
    {
      label: "Lieu de naissance",
      tableView: true,
      key: "lieuDeNaissance",
      type: "textfield",
      input: true,
    },
    {
      type: "button",
      label: "Submit",
      key: "submit",
      disableOnInvalid: true,
      input: true,
      tableView: false,
    },
  ],
};

// export const urlBase = "http://192.168.252.187:8000/api";
export const urlBase = "https://backend-register.odc.ci/api";
export const urlBase1 = "https://formbuilderodc.herokuapp.com/api";
// export const urlBase = "http://192.18.252.196:8000/api";

export const soumission = async (data) => {
  await fetch(urlBase + "/SoumettreFormulaire/", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  }).then(alert("information enregister"));
};
