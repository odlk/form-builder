import React, {Suspense, lazy} from "react";
import {Redirect, Switch, Route} from "react-router-dom";
import {LayoutSplashScreen, ContentRoute} from "../_metronic/layout";
import {BuilderPage} from "./pages/BuilderPage";
import {MyPage} from "./pages/MyPage";
import {CreateFormulaire} from "./pages/Formulaires/CreateFormulaire";
import {ListFormations} from "./pages/Formations/ListFormations";
import { CreateFormations } from "./pages/Formations/CreateFormations";
import {ListFormulaires} from "./pages/Formulaires/ListFormulaires"
import {ListUsers} from "./pages/Utilisateurs/ListUsers"
import {ListSubmit} from "./pages/Soumissions/ListSubmit"
import {CreateUser} from "./pages/Utilisateurs/CreateUser"
import {Modifyer} from "./pages/Formulaires/ModifyFormPage";
import {ModifyFormation} from "./pages/Formations/ModifyFormation";
import {DashboardPage} from "./pages/DashboardPage";
import { Profile } from "./pages/Utilisateurs/profile";
import { ExctractData } from "./pages/Soumissions/ExctractData";

const GoogleMaterialPage = lazy(() =>
  import("./modules/GoogleMaterialExamples/GoogleMaterialPage")
);
const ReactBootstrapPage = lazy(() =>
  import("./modules/ReactBootstrapExamples/ReactBootstrapPage")
);
const ECommercePage = lazy(() =>
  import("./modules/ECommerce/pages/eCommercePage")
);

export default function BasePage() {
    // useEffect(() => {
    //   console.log('Base page');
    // }, []) // [] - is required if you need only one call
    // https://reactjs.org/docs/hooks-reference.html#useeffect

    return (
        <Suspense fallback={<LayoutSplashScreen/>}>
            <Switch>
                {
                    /* Redirect from root URL to /dashboard. */
                    <Redirect exact from="/" to="/dashboard"/>
                }
                <ContentRoute path="/dashboard" component={DashboardPage}/>
                <ContentRoute path="/builder" component={BuilderPage}/>
                <ContentRoute path="/my-page" component={MyPage}/>
                <ContentRoute path="/extract" component={ExctractData}/>
                <ContentRoute path="/CreateFormations" component={CreateFormations} />
                <ContentRoute path="/list-formations" component={ListFormations}/>
                <ContentRoute path="/list-users" component={ListUsers}/>
                <ContentRoute path="/CreateUSer" component={CreateUser}/>
                <ContentRoute path="/ProfileUser" component={Profile}/>
                <ContentRoute path="/CreateFormulaire" component={CreateFormulaire} />
                <ContentRoute path="/list-formulaires" component={ListFormulaires} />
                <ContentRoute path="/modify-form/:id" component={Modifyer} />
                <ContentRoute path="/modify-formation/:id" component={ModifyFormation} />
                <ContentRoute path="/list-submits" component={ListSubmit} />
                <Route path="/google-material" component={GoogleMaterialPage}/>
                <Route path="/react-bootstrap" component={ReactBootstrapPage}/>
                <Route path="/e-commerce" component={ECommercePage}/>
                <Redirect to="error/error-v1"/>
            </Switch>
        </Suspense>
    );
}
