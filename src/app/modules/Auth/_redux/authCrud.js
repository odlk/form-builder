import { useState } from "react";
import axios from "axios";
import { urlBase } from "../../../consts";

export const LOGIN_URL = urlBase+"/login";
export const REGISTER_URL = "api/auth/register";
export const REQUEST_PASSWORD_URL = "api/auth/forgot-password";

export const ME_URL = urlBase+"/profile";

export function login(email, password) {
  return axios.post(LOGIN_URL, { email, password })
}

export const loginUser = async (email, password) => {
  await fetch(urlBase + "/login/", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      email: email,
      password: password,
    }),
  })
};

export function register(email, fullname, username, password) {
  return axios.post(REGISTER_URL, { email, fullname, username, password });
}

export function requestPassword(email) {
  return axios.post(REQUEST_PASSWORD_URL, { email });
}

export function getUserByToken() {
  // Authorization head should be fulfilled in interceptor.
  return axios.get(ME_URL);
}
