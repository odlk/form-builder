import React, { useState, useEffect } from "react";
import { Button, Form } from "react-bootstrap";
import swal from "sweetalert";
import { useSelector } from "react-redux";
import { urlBase } from "../../consts";
import { useHistory } from "react-router-dom";
import { validate } from "react-email-validator";
import { Permission } from "../PermissionService";
import { Service } from "../Service";

export const CreateFormations = () => {
  const [allFormations] = useState([{ formation: "", id_formulaire: "" }]);
  const navigate = useHistory();
  const [IdOptions, setIdOptions] = useState([]);
  const [options, setOptions] = useState([]);
  const [name, setName] = useState();
  const [description, setDescription] = useState();
  const { user } = useSelector((state) => state.auth);
  const Perm = new Permission();
  const FormationService = new Service();
  const handleName = (event) => {
    setName(event.target.value);
  };

  const handleDescription = (event) => {
    setDescription(event.target.value);
    console.log(description);
  };

  const handleOnChange = (event) => {
    setIdOptions(event.target.value);
    console.log(IdOptions);
  };

  const addForm = async () => {
    if (!name || !description || !IdOptions) {
      swal("Tous les champs sont obligatoires", "", "error");
    } else {
      if (typeof name != "string") {
        swal(
          "Le nom d'utilisateur doit être une châine de caracteres",
          "",
          "error"
        );
      }
      if (typeof description != "string") {
        swal("La description doit etre une chaine de caractere");
      }
      if (name && description) {
        if (typeof name == "string" && typeof description == "string") {
          if (user.role) {
            swal("Action reservée aux administrateurs", "", "error");
            navigate.push("/dashboard");
          } else {
            FormationService.addFormation({
              formation: name,
              description: description,
              id_user: String(user.id),
              id_formulaire: IdOptions,
            })
              .then((response) => {
                if (response) {
                  swal("Enregistrement effectué", "!!!", "success");
                  setTimeout(navigate.push("/list-formations"), 10000);
                } else {
                  swal("Enregistrement non effectué", "!!!", "error");
                }
              })
              .catch((err) => console.log(err));
          }
        }
      }
    }
  };
  // const inputChangeHandler = () => {
  //   console.log("OKOKOK");
  // };
  console.log(allFormations);

  useEffect(() => {
    const getFormulaires = async () => {
      const forms = await fetch(urlBase + "/Formulaires").then((res) =>
        res.json()
      );
      console.log(forms);
      setOptions(forms);
    };
    getFormulaires();
  }, []);

  return (
    <div className="card card-custom card-sticky" id="kt_page_sticky_card">
      <div className="card-header">
        <div className="card-title">
          <h3 className="card-label">
            Creation d'une formation<i className="mr-2"></i>
          </h3>
        </div>
      </div>
      <div className="card-body">
        <Form>
          <div className="row">
            <div className="col-xl-2"></div>
            <div className="col-xl-8">
              <div className="my-5">
                <Form.Group
                  className="form-group row"
                  controlId="formBasicEmail"
                >
                  <label className="col-3">Nom de la formation</label>
                  <div className="col-9">
                    <Form.Control
                      type="text"
                      name="name"
                      className="form-control form-control-solid"
                      placeholder="Formation Aws edition 2022"
                      onChange={handleName}
                      required="required"
                    />
                  </div>
                </Form.Group>
                <Form.Group
                  className="form-group row"
                  controlId="formBasicEmail"
                >
                  <label className="col-3">Description de la formation</label>
                  <div className="col-9">
                    <Form.Control
                      as="textarea"
                      rows={3}
                      className="form-control form-control-solid"
                      onChange={handleDescription}
                      required="required"
                    />
                  </div>
                </Form.Group>
                <div className="form-group row">
                  <label className="col-3">choix du formulaire</label>
                  <div className="col-9">
                    <Form.Control
                      as="select"
                      onChange={handleOnChange}
                      className="form-control form-control-solid"
                    >
                      <option>+++++++Liste des formulaires+++++++</option>
                      {options.map(({ nom_form, id }) => (
                        <option key={id} value={id}>
                          {nom_form}
                        </option>
                      ))}
                    </Form.Control>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xl-2"></div>
          </div>
          <div className="card-footer">
            <div class="btn-toolbar">
              <Button
                style={{ backgroundColor: "#F97A07" }}
                className="btn btn-secondary"
                onClick={addForm}
              >
                Enregistrer
              </Button>
              <span style={{ color: "white" }}>rr</span>
              <button
                className="btn btn-secondary"
                onClick={() => navigate.push("/")}
              >
                Annuler
              </button>
            </div>
          </div>
        </Form>
      </div>
    </div>
  );
};
