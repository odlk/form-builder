import React, { useState, useEffect, useRef } from "react";
import { useSubheader } from "../../../_metronic/layout";
import { ConfirmDialog, confirmDialog } from "primereact/confirmdialog";
import { FilterMatchMode, FilterOperator } from "primereact/api";
import { DataTable } from "primereact/datatable";
import { Toolbar } from "primereact/toolbar";
import { Column } from "primereact/column";
import { InputText } from "primereact/inputtext";
import { Dropdown } from "primereact/dropdown";
import { Button } from "primereact/button";
import { Toast } from "primereact/toast";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

import { Service } from "../Service";
import "../DataTableDemo.css";
import { urlBase } from "../../consts";

export const ListFormations = () => {
  const suhbeader = useSubheader();
  suhbeader.setTitle("Creation d'un nouveau formulaire");

  const [formations, setFormations] = useState(null);
  const [filters1, setFilters1] = useState(null);
  const [selectedSubmits, setSelectedSubmits] = useState([]);
  const [filters2, setFilters2] = useState({
    global: { value: null, matchMode: FilterMatchMode.CONTAINS },
    identifiant: { value: null, matchMode: FilterMatchMode.EQUALS },
    formation: { value: null, matchMode: FilterMatchMode.CONTAINS },
    formulaire: { value: null, matchMode: FilterMatchMode.CONTAINS },
    auteur: { value: null, matchMode: FilterMatchMode.CONTAINS },
    created_at: { value: null, matchMode: FilterMatchMode.CONTAINS },
  });
  const [globalFilterValue1, setGlobalFilterValue1] = useState("");
  const [globalFilterValue2, setGlobalFilterValue2] = useState("");
  const [loading2, setLoading2] = useState(true);
  const dt = useRef(null);
  const toast = useRef(null);
  const { user } = useSelector((state) => state.auth);

  const statuses = ["visible", "invisible"];

  const formationsService = new Service();

  useEffect(() => {
    formationsService.getFormation().then((data) => {
      setFormations(getFormations(data));
      setLoading2(false);
    });
    initFilters1();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const getFormations = (data) => {
    return [...(data || [])].map((d) => {
      d.date = new Date(d.date);
      return d;
    });
  };

  const accept = () => {
    toast.current.show({
      severity: "info",
      summary: "Confirmé",
      detail: "Vous avez accepté",
      life: 3000,
    });
  };

  const reject = () => {
    toast.current.show({
      severity: "warn",
      summary: "Annulé",
      detail: "Vous avez refusé",
      life: 3000,
    });
  };
  const confirm1 = () => {
    confirmDialog({
      message: "Vous êtes sûr de vouloir continuer ?",
      header: "Suppression",
      icon: "pi pi-exclamation-triangle",
      accept: () => {
        deleteFormations().then((res) => {
          console.log(res);
          toast.current.show({
            severity: "success",
            summary: "Suppression",
            // detail: res.Message,
            life: 3000,
          });
        });
      },
      reject,
    });
  };

  const confirm2 = (id) => {
    confirmDialog({
      message: "Voulez-vous supprimer cet enregistrement ?",
      header: "Confirmation de la suppression",
      icon: "pi pi-info-circle",
      acceptClassName: "p-button-danger",
      accept: () => {
        formationsService.deleteFormation(id).then((data) => {
          toast.current.show({
            severity: "success",
            summary: "Suppression",
            detail: data.Message,
            life: 3000,
          });
        });
      },
      reject,
    });
  };

  const onGlobalFilterChange2 = (e) => {
    const value = e.target.value;
    let _filters2 = { ...filters2 };
    _filters2["global"].value = value;

    setFilters2(_filters2);
    setGlobalFilterValue2(value);
  };

  const initFilters1 = () => {
    setFilters1({
      global: { value: null, matchMode: FilterMatchMode.CONTAINS },
      identifiant: {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.EQUALS }],
      },
      formation: {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.CONTAINS }],
      },
      formulaire: {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.CONTAINS }],
      },

      auteur: {
        operator: FilterOperator.OR,
        constraints: [{ value: null, matchMode: FilterMatchMode.CONTAINS }],
      },
      created_at: {
        operator: FilterOperator.OR,
        constraints: [{ value: null, matchMode: FilterMatchMode.CONTAINS }],
      },
    });
    setGlobalFilterValue1("");
  };

  const renderHeader2 = () => {
    return (
      <div className="">
        <span className="p-input-icon-left">
          <i className="pi pi-search" />
          <InputText
            value={globalFilterValue2}
            onChange={onGlobalFilterChange2}
            placeholder="Recherche ..."
          />
        </span>
      </div>
    );
  };

  const idBodyTemplate = (rowData) => {
    return <span className="image-text">{rowData.id}</span>;
  };

  const formBodyTemplate = (rowData) => {
    return <span>{rowData.formation}</span>;
  };

  const formulaireBodyTemplate = (rowData) => {
    return <span>{rowData.nom_form}</span>;
  };

  const auteurBodyTemplate = (rowData) => {
    return <span>{rowData.Cree_par}</span>;
  };

  const dateBodyTemplate = (rowData) => {
    return <span>{rowData.date_creation}</span>;
  };

  const statusItemTemplate = (option) => {
    return <span className={`customer-badge status-${option}`}>{option}</span>;
  };

  const exportCSV = () => {
    dt.current.exportCSV();
  };

  const statusRowFilterTemplate = (options) => {
    return (
      <Dropdown
        value={options.value}
        options={statuses}
        onChange={(e) => options.filterApplyCallback(e.value)}
        itemTemplate={statusItemTemplate}
        placeholder="recherche par l'etat"
        className="p-column-filter"
        showClear
      />
    );
  };

  const leftToolbarTemplate = () => {
    if (user.role == "admin") {
      return (
        <React.Fragment>
          <Link to="/CreateFormations">
            <Button
              icon="pi pi-plus-circle"
              label="Créer"
              className="mr-2 p-button-info"
            ></Button>
          </Link>

          <Button
            onClick={confirm1}
            icon="pi pi-trash"
            label="Supprimer"
            className="mr-2 p-button-danger"
            disabled={!selectedSubmits || !selectedSubmits.length}
          ></Button>
        </React.Fragment>
      );
    } else {
      return <></>;
    }
  };

  const rightToolbarTemplate = () => {
    return (
      <React.Fragment>
        <Button
          label="Export"
          icon="pi pi-upload"
          className="p-button-help"
          onClick={exportCSV}
        />
      </React.Fragment>
    );
  };
  const actionBodyTemplate = (rowData) => {
    if (user.role == "admin") {
      return (
        <React.Fragment>
          <Link to={`/modify-formation/${rowData.id}`}>
            <Button
              icon="pi pi-pencil"
              className="p-button-rounded p-button-success mr-2"
            />
          </Link>
          <Button
            onClick={() => {
              confirm2(rowData.id);
            }}
            icon="pi pi-trash"
            className="p-button-rounded p-button-warning"
          />
        </React.Fragment>
      );
    } else {
      return <></>;
    }
  };

  const header2 = renderHeader2();
  const ArrayFormation = [];
  selectedSubmits.map((selecte, index) => {
    ArrayFormation.push(selecte.id);
    console.log(ArrayFormation);
  });
  const deleteFormations = async () => {
    for (let i = 0; i < ArrayFormation.length; i++) {
      await fetch(urlBase + "/formations/" + ArrayFormation[i], {
        method: "DELETE",
      });
    }
  };
  return (
    <div>
      <Toast ref={toast} />
      <div className="datatable-filter-demo">
        <ConfirmDialog />
        <Toolbar
          className="mb-4"
          left={leftToolbarTemplate}
          right={rightToolbarTemplate}
        ></Toolbar>

        <DataTable
          selection={selectedSubmits}
          onSelectionChange={(e) => setSelectedSubmits(e.value)}
          dataKey="id"
          ref={dt}
          value={formations}
          paginator
          className="p-datatable-customers"
          rows={5}
          filters={filters2}
          filterDisplay="row"
          loading={loading2}
          responsiveLayout="scroll"
          globalFilterFields={[
            "identifiant",
            "formation",
            "formulaire",
            "auteur",
            "created_at",
          ]}
          header={header2}
          emptyMessage="Aucune formation"
        >
          <Column
            selectionMode="multiple"
            headerStyle={{ width: "3em" }}
          ></Column>

          <Column
            field="identifiant"
            filterField="identifiant"
            header="#"
            filter
            filterPlaceholder="Recherche par l'identifiant"
            style={{ minWidth: "12rem" }}
            body={idBodyTemplate}
          />
          <Column
            field="formation"
            filterField="formation"
            header="Formation"
            filter
            filterPlaceholder="Recherche par formation"
            style={{ minWidth: "12rem" }}
            body={formBodyTemplate}
          />
          <Column
            field="formulaire"
            filterField="formulaire"
            header="Formulaire"
            filter
            filterPlaceholder="Recherche par formulaire"
            style={{ minWidth: "12rem" }}
            body={formulaireBodyTemplate}
          />
          <Column
            filterField="auteur"
            field="auteur"
            header="Auteur"
            showFilterMenu={false}
            style={{ minWidth: "12rem" }}
            body={auteurBodyTemplate}
            filter
            filterPlaceholder="Recherche par auteur"
          />
          <Column
            field="created_at"
            header="Date de creation"
            filterField="created_at"
            showFilterMenu={false}
            filterMenuStyle={{ width: "14rem" }}
            style={{ minWidth: "14rem" }}
            body={dateBodyTemplate}
            filter
            filterPlaceholder="Recherche par date"
          />
          <Column
            body={actionBodyTemplate}
            exportable={false}
            style={{ minWidth: "8rem" }}
          ></Column>
        </DataTable>
      </div>
    </div>
  );
};
