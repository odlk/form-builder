import React, { useState, useEffect } from "react";
import { Container, Row, Col, Button, Form } from "react-bootstrap";
import { Route, Link, Routes, useParams } from "react-router-dom";
import { formIoData, urlBase } from "../../consts";
import { useSelector } from "react-redux";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import { Service } from "../Service";

export const ModifyFormation = () => {
  // const [IdOptions, setIdOptions] = useState([])
  // const [options, setOptions] = useState([])
  // const [name,setName]=useState()

  const params = useParams();
  const [formations, setFormations] = useState([]);
  const navigate = useHistory();

  const [options, setOptions] = useState([]);
  const [name, setName] = useState();
  const [description, setDescription] = useState();
  const { user } = useSelector((state) => state.auth);
  const [valueName, setValuename] = useState(0);
  const [valueDescription, setValueDescription] = useState(0);
  const [valueId, setValueId] = useState(0);
  const formationService = new Service();
  const getFormulaires = async () => {
    const formulaires = await fetch(urlBase + "/Formulaires").then((res) =>
      res.json()
    );
    setOptions(formulaires);
  };

  const getFormations = async () => {
    const formationsById = await fetch(
      `${urlBase}/formations/${params.id}`
    ).then((res) => res.json());
    setFormations(formationsById);
  };

  var id_formulaire_static = formations.id_formulaire;
  const [IdOptions, setIdOptions] = useState();

  const handleName = (event) => {
    formations.formation = setName(event.target.value);
    // console.log(event.target.value)
    setName(event.target.value);
    setValuename(1);
    console.log(name);
  };
  const handleDescription = (event) => {
    formations.description = setDescription(event.target.value);
    setDescription(event.target.value);
    setValueDescription(1);
    console.log(description);
  };
  const handleOnChange = (event) => {
    formations.id = setIdOptions(event.target.value);
    setIdOptions(event.target.value);
    setValueId(1);
    console.log(IdOptions);
  };
  const EditForm = async () => {
    if (valueName == 0) {
      var nameValue = formations.formation;
    } else {
      nameValue = name;
    }
    if (valueDescription == 0) {
      var descriptionValue = formations.description;
    } else {
      descriptionValue = description;
    }
    if (valueId == 0) {
      var idValue = formations.id;
    } else {
      idValue = IdOptions;
    }
    if (!nameValue || !descriptionValue || !idValue) {
      swal("Tous les champs sont obligatoires", "", "error");
    } else {
      if (typeof nameValue != "string") {
        swal(
          "Le nom de la formation doit etre une chaine de caractere",
          "",
          "error"
        );
      }
      if (typeof descriptionValue != "string") {
        swal(
          "La description de la formation doit être une chaine de caractere",
          "",
          "error"
        );
      }

      if (nameValue && descriptionValue) {
        if (
          typeof nameValue == "string" &&
          typeof descriptionValue == "string"
        ) {
          if (user.role != "admin") {
            swal("Action reservée aux administrateurs", "", "error");
          } else {
            formationService
              .editFormation(params.id, {
                formation: nameValue,
                description: descriptionValue,
                id_user: user.id,
                id_formulaire: idValue,
              })
              .then((response) => {
                if (response) {
                  swal("Modification effectuée", "!!!", "success");
                  setTimeout(navigate.push("/list-formations"), 10000);
                } else {
                  swal("La modification n'a pas été effectuée", "!!!", "error");
                }
              })
              .catch((err) => console.log(err));
          }
        }
      }
    }
  };

  useEffect(() => {
    getFormulaires();
    getFormations();
  }, []);

  // console.log(formations)

  return (
    <div className="card card-custom card-sticky" id="kt_page_sticky_card">
      <div className="card-header">
        <div className="card-title">
          <h3 className="card-label">
            modification d'une formation<i className="mr-2"></i>
          </h3>
        </div>
      </div>
      <div className="card-body">
        <Form>
          <div className="row">
            <div className="col-xl-2"></div>
            <div className="col-xl-8">
              <div className="my-5">
                <Form.Group
                  className="form-group row"
                  controlId="formBasicEmail"
                >
                  <label className="col-3">Nom de la formation</label>
                  <div className="col-9">
                    <Form.Control
                      type="text"
                      name="name"
                      className="form-control form-control-solid"
                      placeholder="Formation Aws edition 2022"
                      value={formations.formation}
                      onChange={handleName}
                      required="required"
                    />
                  </div>
                </Form.Group>
                <Form.Group
                  className="form-group row"
                  controlId="formBasicEmail"
                >
                  <label className="col-3">Description de la formation</label>
                  <div className="col-9">
                    <Form.Control
                      as="textarea"
                      rows={3}
                      className="form-control form-control-solid"
                      onChange={handleDescription}
                      value={formations.description}
                      required="required"
                    />
                  </div>
                </Form.Group>
                <div className="form-group row">
                  <label className="col-3">choix du formulaire</label>
                  <div className="col-9">
                    <Form.Control
                      as="select"
                      onChange={handleOnChange}
                      className="form-control form-control-solid"
                    >
                      <option
                        key={formations.id}
                        value={formations.id}
                        selected
                      >
                        ----------------------------vous aviez choisi:
                        {formations.nom_form}----------------------------
                      </option>
                      <option key={0} value={0}></option>
                      {options.map(({ nom_form, id }) => (
                        <option key={id} value={id}>
                          {nom_form}
                        </option>
                      ))}
                    </Form.Control>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xl-2"></div>
          </div>
          <div className="card-footer">
            <div class="btn-toolbar">
              <Button
                style={{ backgroundColor: "#F97A07" }}
                className="btn btn-secondary"
                onClick={EditForm}
              >
                Modifier
              </Button>
              <span style={{ color: "white" }}>rr</span>
              <button
                className="btn btn-secondary"
                onClick={() => navigate.push("/list-formations")}
              >
                Annuler
              </button>
            </div>
          </div>
        </Form>
      </div>
    </div>
  );
};
