import React, { useState, useEffect, useRef } from "react";
import { useSubheader } from "../../_metronic/layout";
import { ConfirmDialog, confirmDialog } from "primereact/confirmdialog";
import { FilterMatchMode, FilterOperator } from "primereact/api";
import { DataTable } from "primereact/datatable";
import { Toolbar } from "primereact/toolbar";
import { Column } from "primereact/column";
import { InputText } from "primereact/inputtext";
import { Dropdown } from "primereact/dropdown";
import { Button } from "primereact/button";
import { Toast } from "primereact/toast";

import { Service } from "./Service";
import "./DataTableDemo.css";

export const MyPage = () => {
  const suhbeader = useSubheader();
  suhbeader.setTitle("Creation d'un nouveau formulaire");

  const [soumission, setSoumission] = useState(null);
  const [filters1, setFilters1] = useState(null);
  const [selectedSubmits, setSelectedSubmits] = useState(null);
  const [filters2, setFilters2] = useState({
    global: { value: null, matchMode: FilterMatchMode.CONTAINS },
    formations: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
    formulaire: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
    status: { value: null, matchMode: FilterMatchMode.EQUALS },
  });
  const [globalFilterValue1, setGlobalFilterValue1] = useState("");
  const [globalFilterValue2, setGlobalFilterValue2] = useState("");
  const [loading2, setLoading2] = useState(true);
  const dt = useRef(null);
  const toast = useRef(null);

  const statuses = ["validé", "non validé"];

  const soumissionService = new Service();

  useEffect(() => {
    soumissionService.getSubmit().then((data) => {
      setSoumission(getSubmits(data));
      setLoading2(false);
    });
    initFilters1();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const getSubmits = (data) => {
    return [...(data || [])].map((d) => {
      d.date = new Date(d.date);
      return d;
    });
  };

  const accept = () => {
    toast.current.show({
      severity: "info",
      summary: "Confirmé",
      detail: "Vous avez accepté",
      life: 3000,
    });
  };

  const reject = () => {
    toast.current.show({
      severity: "warn",
      summary: "Annulé",
      detail: "Vous avez refusé",
      life: 3000,
    });
  };
  
  const confirm1 = () => {
    confirmDialog({
      message: "Vous êtes sûr de vouloir continuer ?",
      header: "Validation",
      icon: "pi pi-exclamation-triangle",
      accept,
      reject,
    });
  };
  const onGlobalFilterChange2 = (e) => {
    const value = e.target.value;
    let _filters2 = { ...filters2 };
    _filters2["global"].value = value;

    setFilters2(_filters2);
    setGlobalFilterValue2(value);
  };

  const initFilters1 = () => {
    setFilters1({
      global: { value: null, matchMode: FilterMatchMode.CONTAINS },
      formations: {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.STARTS_WITH }],
      },
      formulaire: {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.STARTS_WITH }],
      },

      status: {
        operator: FilterOperator.OR,
        constraints: [{ value: null, matchMode: FilterMatchMode.EQUALS }],
      },
    });
    setGlobalFilterValue1("");
  };

  const renderHeader2 = () => {
    return (
      <div className="">
        <span className="p-input-icon-left">
          <i className="pi pi-search" />
          <InputText
            value={globalFilterValue2}
            onChange={onGlobalFilterChange2}
            placeholder="Recherche ..."
          />
        </span>
      </div>
    );
  };

  const countryBodyTemplate = (rowData) => {
    return <span className="image-text">{rowData.formulaire}</span>;
  };

  const dateBodyTemplate = (rowData) => {
    return <span>{rowData.created_at}</span>;
  };

  const statusBodyTemplate = (rowData) => {
    return (
      <span className={`customer-badge status-${rowData.status}`}>
        {rowData.status}
      </span>
    );
  };

  const statusItemTemplate = (option) => {
    return <span className={`customer-badge status-${option}`}>{option}</span>;
  };

  const exportCSV = () => {
    dt.current.exportCSV();
  };

  const statusRowFilterTemplate = (options) => {
    return (
      <Dropdown
        value={options.value}
        options={statuses}
        onChange={(e) => options.filterApplyCallback(e.value)}
        itemTemplate={statusItemTemplate}
        placeholder="Recherche par status"
        className="p-column-filter"
        showClear
      />
    );
  };

  const leftToolbarTemplate = () => {
    return (
      <React.Fragment>
        <Button
          onClick={confirm1}
          icon="pi pi-check"
          label="Valider"
          className="mr-2 p-button-success"
          disabled={!selectedSubmits || !selectedSubmits.length}
        ></Button>
      </React.Fragment>
    );
  };

  const rightToolbarTemplate = () => {
    return (
      <React.Fragment>
        <Button
          label="Export"
          icon="pi pi-upload"
          className="p-button-help"
          onClick={exportCSV}
        />
      </React.Fragment>
    );
  };

  const header2 = renderHeader2();

  return (
    <div>
      <Toast ref={toast} />
      <div className="datatable-filter-demo">
        <ConfirmDialog />
        <Toolbar
          className="mb-4"
          left={leftToolbarTemplate}
          right={rightToolbarTemplate}
        ></Toolbar>

        <DataTable
          selection={selectedSubmits}
          onSelectionChange={(e) => setSelectedSubmits(e.value)}
          dataKey="id"
          ref={dt}
          value={soumission}
          paginator
          className="p-datatable-customers"
          rows={10}
          filters={filters2}
          filterDisplay="row"
          loading={loading2}
          responsiveLayout="scroll"
          globalFilterFields={[
            "formations",
            "formulaire",
            "created_at",
            "status",
          ]}
          header={header2}
          emptyMessage="Aucune soumission"
        >
          <Column
            selectionMode="multiple"
            headerStyle={{ width: "3em" }}
          ></Column>

          <Column
            field="formations"
            header="Formation"
            filter
            filterPlaceholder="Recherche par formations"
            style={{ minWidth: "12rem" }}
          />
          <Column
            header="Formulaire"
            filterField="formulaire"
            style={{ minWidth: "12rem" }}
            body={countryBodyTemplate}
            filter
            filterPlaceholder="Recherche par formulaire"
          />

          <Column
            field="status"
            header="Status"
            showFilterMenu={false}
            filterMenuStyle={{ width: "14rem" }}
            style={{ minWidth: "12rem" }}
            body={statusBodyTemplate}
            filter
            filterElement={statusRowFilterTemplate}
          />
          <Column
            header="Date de creation"
            filterField="created_at"
            showFilterMenu={false}
            filterMenuStyle={{ width: "14rem" }}
            style={{ minWidth: "14rem" }}
            body={dateBodyTemplate}
          />
        </DataTable>
      </div>
    </div>
  );
};
