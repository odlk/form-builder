import Axios from "axios";
import React, { useEffect, useState } from "react";
import swal from "sweetalert";
import { ConfirmDialog, confirmDialog } from "primereact/confirmdialog";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Service } from "../Service";
import { DownloadExcel } from "react-excel-export";
import { For } from "react-loops";






export const ExctractData = () => {


  const dataService = new Service();
  const [champs, setChamps] = useState();
  const [operateur, setOperateur] = useState([]);
  const [valeur, setValeur] = useState();
  const [dataInit, setDataInit] = useState([]);
  const [testData,setTestData]=useState([])
  const [columns , setColumn] = useState([])
  const handleChamps = (event) => {
    setChamps(event.target.value);
  };
  const tb=[]
  const tb1=[]
  const newColumn = []
  const dataValue = []
  const [newTb,setNewTb]=useState([])

  const fileType="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8"
  const fileExtension=".xlsx"
  const fileName="test"
  const handleOperateur = (event) => {
    setOperateur(event.target.value);
  };

  const handleValeur = (event) => {
    setValeur(event.target.value);
  };



  useEffect(() => {
    // testData();
    const params = champs + operateur + valeur;
    dataService.Search(params);

    dataService.getSubmit().then((datas)=>{
      console.log(datas);
      setDataInit(datas)
    })
  }, []);



  const data=(table,source)=>{

    source.map(item=>{
      table.push(item.submit)
    })

    table.forEach(( item) =>{
      if(newColumn.length < 1){
        newColumn.push(Object.keys(item))
      }
        dataValue.push(Object.values(item))
      
    })
  }



  data(tb,dataInit)
  // console.log(dataInit.length)

  const onSearch = () => {
    const params = champs + operateur + valeur;
    // console.log(params);
    dataService.Search(params).then((res) => {
      // console.log(data);
      setTestData(res)
      
    }
    
    );
  };
  console.log(tb)
  console.log(testData.length)

  if(testData.length>0){
    testData.map(item=>{
      tb1.push(item._source.submit)
    })
    for(let i=0; i<tb.length; i++){
      tb.pop()
    }
    tb1.forEach(( item) =>{
      if(newColumn.length < 1){
        newColumn.push(Object.keys(item))
      }
        dataValue.push(Object.values(item))
      
    })
  }
  console.log(tb1)

  return (
    <>
      <div className="container">
        <div className="card card-custom">
          <div className="card-header">
            <div className="card-title">Formulaire de recherche avancé</div>
            <div className="card-toolbar">
              <div className="dropdown dropdown-inline mr-2">

       <DownloadExcel
        data={tb1}
        buttonLabel="Exporter les données"
        fileName="export"
        className="export-button"
      />

                <div className="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                  <ul className="navi flex-column navi-hover py-2">
                    <li className="navi-header font-weight-bolder text-uppercase font-size-sm text-primary pb-2">
                      Choose an option:
                    </li>
                    <li className="navi-item">
                      <a href="#" className="navi-link">
                        <span className="navi-icon">
                          <i className="la la-print"></i>
                        </span>
                        <span className="navi-text">Print</span>
                      </a>
                    </li>
                    <li className="navi-item">
                      <a href="#" className="navi-link">
                        <span className="navi-icon">
                          <i className="la la-copy"></i>
                        </span>
                        <span className="navi-text">Copy</span>
                      </a>
                    </li>
                    <li className="navi-item">
                      <a href="#" className="navi-link">
                        <span className="navi-icon">
                          <i className="la la-file-excel-o"></i>
                        </span>
                        <span className="navi-text">Excel</span>
                      </a>
                    </li>
                    <li className="navi-item">
                      <a href="#" className="navi-link">
                        <span className="navi-icon">
                          <i className="la la-file-text-o"></i>
                        </span>
                        <span className="navi-text">CSV</span>
                      </a>
                    </li>
                    <li className="navi-item">
                      <a href="#" className="navi-link">
                        <span className="navi-icon">
                          <i className="la la-file-pdf-o"></i>
                        </span>
                        <span className="navi-text">PDF</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div className="card-body">
            <form className="kt-form kt-form--fit mb-15">
              <div class="row mb-8">
                <div class="col-lg-3 mb-lg-0 mb-6">
                  <div class="datatable-input input-group" id="kt_datepicker">
                    <input
                      type="text"
                      class="form-control datatable-input"
                      name="start"
                      placeholder="exemple:age"
                      onChange={handleChamps}
                      data-col-index="5"
                    />
                    <div class="input-group-append"></div>
                  </div>
                </div>
                <div class="col-lg-3 mb-lg-0 mb-6">
                  <select
                    class="form-control datatable-input"
                    data-col-index="6"
                    onChange={handleOperateur}
                  >
                    <option value="">Opérateur de compraison</option>
                    <option value=":>">Supérieur</option>
                    <option value=":<">Inférieur</option>
                    <option value=":">Egale</option>
                  </select>
                </div>
                <div class="col-lg-3 mb-lg-0 mb-6">
                  <input
                    type="text"
                    class="form-control datatable-input"
                    name="start"
                    placeholder="exemple:age"
                    data-col-index="5"
                    onChange={handleValeur}
                  />
                </div>
                <div class="col-lg-3 mb-lg-0 mb-6">
                  <a
                    href="#"
                    onClick={onSearch}
                    className="btn btn-primary font-weight-bolder"
                  >
                    <span className="svg-icon svg-icon-md"></span>Rechercher
                  </a>
                </div>
              </div>
            </form>
            <div>
              <div className="datatable-filter-demo">
              <div>
            <div className="card">
                {/* <DataTable value={newColumn} responsiveLayout="scroll">
                {
                  newColumn.map((item)=>{
                  return  <Column field={item} header={item}></Column>
                  })
                }
                </DataTable> */}
                <table>
                  <tr>
                    {
                      newColumn.map((item)=>{
                        return <th>{item}</th>
                      })
                    }
                  </tr>
                  {
                    dataValue.map((item)=>{
                     return <tr><td>{item}</td></tr>
                    })
                  }
                </table>
            </div>
        </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
