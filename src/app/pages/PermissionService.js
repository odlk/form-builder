import swal from "sweetalert";
import { urlBase } from "../consts";
import { useHistory } from "react-router-dom";

export class Permission {
  async getUserRole(id) {
    const navigate = useHistory();
    const role = await fetch(urlBase + "/Users/" + id).then((res) =>
      res.json()
    );
    const roleUSer = role;
    if (roleUSer.role != "admin") {
      navigate.push("/dashboard");
      swal("cette opération est reservée aux Administrateurs", "", "error");
    }
  }
}
