import React, { useState, useEffect } from "react";
// import { ReactFormBuilder, ReactFormGenerator } from "react-form-builder2";
import { FormBuilder as FormBuilderIo, Formio } from "react-formio";
import { formIoData, soumission, urlBase } from "../../consts";
import "../../styles.css";
import "react-form-builder2/dist/app.css";
import "formiojs/dist/formio.full.css";
import { makeStyles } from "@material-ui/core/styles";
import swal from "sweetalert";
import { useSelector } from "react-redux";
import { Button, Form } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { Service } from "../Service";

const useStyles = makeStyles((theme) => ({
  button: {
    margin: theme.spacing(1),
  },
  input: {
    display: "none",
  },
}));

export const CreateFormulaire = () => {
  // State des GET ET POST
  const [formData, setFormData] = useState(formIoData);
  const [nom, setNomForm] = useState("");
  const { user } = useSelector((state) => state.auth);
  const navigate = useHistory();
  const FormulaireSevice = new Service();
  const classes = useStyles();

  const handleNameFormulaire = (event) => {
    setNomForm(event.target.value);
  };

  const AddForm = async () => {
    if (!nom || !formIoData.components) {
      swal("Tous les champs sont obligatoires", "", "error");
    } else {
      if (typeof nom != "string") {
        swal("Le nom de la formation doit etre une chaine de caractere");
      }
      if (nom && formIoData.components) {
        if (typeof nom == "string") {
          if (user.role) {
            swal("Action reservée aux administrateurs", "", "error");
            navigate.push("/dashboard");
          } else {
            FormulaireSevice.addFormulaire({
              structure_form: formIoData.components,
              visible: 1,
              nom_form: nom,
              id_user: user.id,
            })
              .then((response) => {
                if (response) {
                  swal("Enregistrement effectué", "!!!", "success");
                  setTimeout(navigate.push("/list-formulaires"), 10000);
                } else {
                  swal("Enregistrement non effectué", "!!!", "error");
                }
              })
              .catch((err) => console.log(err));
          }
        }
      }
    }
  };

  const printResult = () => {
    Formio.createForm(document.getElementById("formio-result"), {
      components: formData.components,
    }).then((form) => {
      // console.log(form.component.components);
      form.on("submit", (data) => soumission({ submit: data.data }));
      //console.log(document.getElementById("formio-result"));
    });
    // }
  };
  return (
    <div className="card card-custom card-sticky" id="kt_page_sticky_card">
      <div className="card-header">
        <div className="card-title">
          <h3 className="card-label">
            Creation d'un formulaire<i className="mr-2"></i>
          </h3>
        </div>
      </div>
      <div className="card-body">
        <Form>
          <div className="my-5">
            <Form.Group className="form-group row" controlId="formBasicEmail">
              <label className="col-2">Nom du formulaire</label>
              <div className="col-10">
                <Form.Control
                  type="text"
                  name="name"
                  className="form-control form-control-solid"
                  placeholder="Formulaire formation AWS"
                  onChange={handleNameFormulaire}
                  required="required"
                />
              </div>
            </Form.Group>
          </div>
        </Form>
        <FormBuilderIo
          form={formIoData}
          //onChange={schema => setFormData(schema)}
          onSubmit={(data) => {
            console.log(data);
          }}
          saveForm={(data) => setFormData(data)}
          saveText="Save Form"
          onSubmitDone={(data) => console.log(data)}
        />
      </div>
      <div class="btn-toolbar ml-9">
        <Button
          style={{ backgroundColor: "#F97A07" }}
          className="btn btn-secondary"
          onClick={AddForm}
        >
          Enregistrer
        </Button>
        <span style={{ color: "white" }}>rr</span>
        <button
          className="btn btn-secondary"
          onClick={() => navigate.push("/")}
        >
          Annuler
        </button>
      </div>
    </div>
  );
};
