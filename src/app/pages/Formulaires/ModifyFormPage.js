import React, { useState, useEffect } from "react";
// import { ReactFormBuilder, ReactFormGenerator } from "react-form-builder2";
import { FormBuilder as FormBuilderIo, Formio } from "react-formio";
import { formIoData, urlBase } from "../../consts";
import "../../styles.css";
import "react-form-builder2/dist/app.css";
import "formiojs/dist/formio.full.css";
import { makeStyles } from "@material-ui/core/styles";
import { useParams } from "react-router-dom";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import { Button, Form } from "react-bootstrap";
import { useSelector } from "react-redux";
import { Service } from "../Service";

const useStyles = makeStyles((theme) => ({
  button: {
    margin: theme.spacing(1),
  },
  input: {
    display: "none",
  },
}));

export const Modifyer = () => {
  //const [formData, setFormData] = useState(formIoData);

  const classes = useStyles();
  const [forms, setForms] = useState([]);
  const [formData, setFormData] = useState(formIoData);
  const navigate = useHistory();
  const [name, setName] = useState();
  const { user } = useSelector((state) => state.auth);
  const FormulaireSevice = new Service();

  //Validation de données
  const [valueNomForm, setValueNomForm] = useState(0);
  // const [valueComponents, setValueComponents]=useState(0)

  const handleName = (e) => {
    forms.nom_form = setName(e.target.value);
    setName(e.target.value);
    setValueNomForm(1);
    console.log(name);
  };
  const params = useParams();
  console.log(params);
  const fetchData = () => {
    fetch(`${urlBase}/Formulaires/${params.id}`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setForms(data);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  const formDataNew = {
    display: "form",
    components: forms["structure_form"],
  };
  //   formDataNew.components=forms['structure_form']
  //   formDataNew.display="form"
  console.log(formDataNew);

  const EditForm = async () => {
    if (valueNomForm == 0) {
      var nomForm = forms.nom_form;
    } else {
      nomForm = name;
    }

    if (!nomForm) {
      swal("Tous les champs sont obligatoires", "", "error");
    } else {
      if (typeof nomForm != "string") {
        swal(
          "Le nom de la formation doit être une chaine de caraceteres",
          "",
          "error"
        );
      }
      if (nomForm) {
        if (typeof nomForm == "string") {
          if (user.role != "admin") {
            swal("Action reservée aux administrateurs", "", "error");
          } else {
            FormulaireSevice.editFormulaire(
              {
                nom_form: nomForm,
                structure_form: formDataNew.components,
                visible: 1,
                cree_par: forms.cree_par,
              },
              params.id
            ).then((result) => {
              if (result) {
                swal("Modification effectuée", "!!!", "success");
                setTimeout(navigate.push("/list-formulaires"), 10000);
              } else {
                swal("La modification n'a pas été effectuée", "!!!", "error");
              }
            });
          }
        }
      }
    }
  };

  const printResult = () => {
    Formio.createForm(document.getElementById("formio-result"), {
      components: formDataNew.components,
    }).then((form) => {
      form.on("submit", (data) => console.log(data.data));
    });
    // }
  };
  return (
    <div className="card card-custom card-sticky" id="kt_page_sticky_card">
      <div className="card-header">
        <div className="card-title">
          <h3 className="card-label">
            Modification de formulaire<i className="mr-2"></i>
          </h3>
        </div>
      </div>
      <div className="card-body">
        <Form>
          <div className="my-5">
            <Form.Group className="form-group row" controlId="formBasicEmail">
              <label className="col-2">Nom du formulaire</label>
              <div className="col-10">
                <Form.Control
                  type="text"
                  name="name"
                  className="form-control form-control-solid"
                  placeholder="Formulaire formation AWS"
                  value={forms.nom_form}
                  onChange={handleName}
                  required="required"
                />
              </div>
            </Form.Group>
          </div>
        </Form>
        <FormBuilderIo
          form={formDataNew}
          //onChange={schema => setFormData(schema)}
          onSubmit={(data) => {
            console.log(data);
          }}
          saveForm={(data) => setFormData(data)}
          saveText="Save Form"
          onSubmitDone={(data) => console.log(data)}
        />
      </div>
      <div class="btn-toolbar ml-9">
        <Button
          style={{ backgroundColor: "#F97A07" }}
          className="btn btn-secondary"
          onClick={EditForm}
        >
          Modifier
        </Button>
        <span style={{ color: "white" }}>rr</span>
        <button
          className="btn btn-secondary"
          onClick={() => navigate.push("/")}
        >
          Annuler
        </button>
      </div>
    </div>
  );
};
