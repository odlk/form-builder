import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Button, Form } from "react-bootstrap";
import swal from "sweetalert";
import { validate } from "react-email-validator";
import { urlBase } from "../../consts";
import { useHistory } from "react-router-dom";
import { Service } from "../Service";

export const Profile = () => {
  const { user } = useSelector((state) => state.auth);
  const [email, setEmail] = useState();
  const [userName, setUsername] = useState();
  const [fullName, setFullName] = useState();
  const [password, setPassword] = useState();
  const [confirmPassword, setConfirmPassword] = useState();
  const history = useHistory();
  const profile = new Service();
  const modifyUser = () => {
    if (!email || !userName || !password || !confirmPassword) {
      swal("Tous les champs sont obligatoires", "!!!", "error");
    } else {
      if (typeof userName != "string") {
        swal(
          "le nom d'utilisateur doit etre une chaine de caractere",
          "",
          "error"
        );
      }
      if (password != confirmPassword) {
        swal("Les mots de passe ne sont pas identques", "", "error");
      }
      if (validate(email) != true) {
        swal("le mail n'est pas valide", "", "error");
      }
      if (email && userName && password && confirmPassword) {
        if (
          typeof userName == "string" &&
          password == confirmPassword &&
          validate(email) == true
        ) {
          profile
            .EditUser(user.id, {
              name: userName,
              email: email,
              password: password,
            })
            .then((response) => {
              if (response) {
                swal("Modifications apportées", "", "success");
                history.push("/dashboard");
              } else {
                swal("Erreur de modification", "", "error");
              }
            })
            .catch((err) => console.log(err));
        }
      }
    }
  };
  const handleEmail = (e) => {
    // console.log(e.target.value)
    setEmail(e.target.value);
  };
  const handleUserName = (e) => {
    // console.log(e.target.value)
    setUsername(e.target.value);
  };
  const handlePassword = (e) => {
    setPassword(e.target.value);
  };
  const handleConfirmPassword = (e) => {
    // console.log(e.target.value)
    setConfirmPassword(e.target.value);
  };

  const handleFullName = (e) => {
    // console.log(e.target.value)
    setFullName(e.target.value);
  };

  useEffect(() => {}, []);

  return (
    <>
      <Form>
        <div class="container">
          <div class="d-flex flex-row">
            <div
              class="flex-row-auto offcanvas-mobile w-250px w-xxl-350px"
              id="kt_profile_aside"
            >
              <div class="card card-custom card-stretch">
                <div class="card-body pt-4">
                  <div class="d-flex align-items-center">
                    <div class="symbol symbol-60 symbol-xxl-100 mr-5 align-self-start align-self-xxl-center">
                      <div class="symbol-label"></div>
                      <i class="symbol-badge bg-success"></i>
                    </div>
                    <div>
                      <a
                        href="#"
                        class="font-weight-bolder font-size-h5 text-dark-75 text-hover-primary"
                      ></a>
                      <div class="text-muted"></div>
                    </div>
                  </div>

                  <div class="py-9">
                    <div class="d-flex align-items-center justify-content-between mb-2">
                      <span class="font-weight-bold mr-2">
                        Nom d'utilisateur:
                      </span>
                      <a href="#" class="text-muted text-hover-primary">
                        {user.username}
                      </a>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-2">
                      <span class="font-weight-bold mr-2">Email:</span>
                      <a href="#" class="text-muted text-hover-primary">
                        {user.email}
                      </a>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-2">
                      <span class="font-weight-bold mr-2">Nom complet:</span>
                      <a href="#" class="text-muted text-hover-primary">
                        {user.fullname}
                      </a>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-2">
                      <span class="font-weight-bold mr-2">Role:</span>
                      <a href="#" class="text-muted text-hover-primary">
                        {user.role}
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="flex-row-fluid ml-lg-8">
              <div class="card card-custom card-stretch">
                <div class="card-header py-3">
                  <div class="card-title align-items-start flex-column">
                    <h3 class="card-label font-weight-bolder text-dark">
                      Informations personelles
                    </h3>
                    <span class="text-muted font-weight-bold font-size-sm mt-1">
                      modifiez vos informations personelles
                    </span>
                  </div>
                  <div class="card-toolbar">
                    <Button
                      className="btn btn-success mr-2"
                      onClick={modifyUser}
                    >
                      Enregistrer les modifications
                    </Button>
                    <button type="reset" class="btn btn-secondary">
                      Annuler
                    </button>
                  </div>
                </div>
                <div className="card-body">
                  <div className="row">
                    <div className="col-xl-12">
                      <div className="my-5">
                        <Form.Group
                          className="form-group row"
                          controlId="formBasicEmail"
                        >
                          <label className="col-4">Nom d'utilisateur</label>
                          <div className="col-8">
                            <Form.Control
                              type="text"
                              name="name"
                              className="form-control form-control-solid"
                              placeholder="John Doe"
                              onChange={handleUserName}
                              required="required"
                            />
                          </div>
                        </Form.Group>
                        {/* <Form.Group
                className="form-group row"
                controlId="formBasicEmail"
              >
                <label className="col-4">Nom et prénoms</label>
                <div className="col-8">
                  <Form.Control
                    type="text"
                    name="fullName"
                    className="form-control form-control-solid"
                    placeholder="John Doe"
                    onChange={handleFullName}
                    required="required"
                  />
                </div>
              </Form.Group> */}
                        <Form.Group
                          className="form-group row"
                          controlId="formBasicEmail"
                        >
                          <label className="col-4">Email</label>
                          <div className="col-8">
                            <Form.Control
                              type="email"
                              name="email"
                              className="form-control form-control-solid"
                              placeholder="test@gmail.com"
                              onChange={handleEmail}
                              required="required"
                            />
                          </div>
                        </Form.Group>
                        <Form.Group
                          className="form-group row"
                          controlId="formBasicEmail"
                        >
                          <label className="col-4">Nouveau mot de passe</label>
                          <div className="col-8">
                            <Form.Control
                              type="password"
                              name="password"
                              className="form-control form-control-solid"
                              required="required"
                              onChange={handlePassword}
                            />
                          </div>
                        </Form.Group>
                        <Form.Group
                          className="form-group row"
                          controlId="formBasicEmail"
                        >
                          <label className="col-4">
                            Confirmation de mot de passe
                          </label>
                          <div className="col-8">
                            <Form.Control
                              type="password"
                              name="password"
                              className="form-control form-control-solid"
                              onChange={handleConfirmPassword}
                              required="required"
                            />
                          </div>
                        </Form.Group>
                      </div>
                    </div>
                    <div className="col-xl-2"></div>
                  </div>
                  <div className="card-footer"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Form>
    </>
  );
};
