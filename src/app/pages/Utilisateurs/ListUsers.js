import React, { useState, useEffect, useRef } from "react";
import { useSubheader } from "../../../_metronic/layout";
import { ConfirmDialog, confirmDialog } from "primereact/confirmdialog";
import { FilterMatchMode, FilterOperator } from "primereact/api";
import { DataTable } from "primereact/datatable";
import { Toolbar } from "primereact/toolbar";
import { Column } from "primereact/column";
import { InputText } from "primereact/inputtext";
import { Dropdown } from "primereact/dropdown";
import { Button } from "primereact/button";
import { Toast } from "primereact/toast";
import { Link } from "react-router-dom";
import { Service } from "../Service";
import "../DataTableDemo.css";
import { urlBase } from "../../consts";
import { useSelector } from "react-redux";

export const ListUsers = () => {
  const suhbeader = useSubheader();
  suhbeader.setTitle("Creation d'un nouveau formulaire");

  const [users, setUsers] = useState(null);
  const { user } = useSelector((state) => state.auth);
  const [filters1, setFilters1] = useState(null);
  const [selectedSubmits, setSelectedSubmits] = useState([]);
  const [filters2, setFilters2] = useState({
    global: { value: null, matchMode: FilterMatchMode.CONTAINS },
    identifiant: { value: null, matchMode: FilterMatchMode.EQUALS },
    username: { value: null, matchMode: FilterMatchMode.CONTAINS },
    email: { value: null, matchMode: FilterMatchMode.CONTAINS },
    role: { value: null, matchMode: FilterMatchMode.EQUALS }
  });
  const [globalFilterValue1, setGlobalFilterValue1] = useState("");
  const [globalFilterValue2, setGlobalFilterValue2] = useState("");
  const [loading2, setLoading2] = useState(true);
  const dt = useRef(null);
  const toast = useRef(null);

  const statuses = ["admin", "editeur"];

  const Users = new Service();

  useEffect(() => {
    Users.getUsers().then((data) => {
      setUsers(getUser(data));
      setLoading2(false);

    });
    initFilters1();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const getUser = (data) => {
    return [...(data || [])].map((d) => {
      d.date = new Date(d.date);
      return d;
    });
  };

  const accept = () => {
    toast.current.show({
      severity: "info",
      summary: "Confirmé",
      detail: "Vous avez accepté",
      life: 3000,
    });
  };

  const reject = () => {
    toast.current.show({
      severity: "warn",
      summary: "Annulé",
      detail: "Vous avez refusé",
      life: 3000,
    });
  };
  const confirm1 = () => {
    confirmDialog({
      message: "Vous êtes sûr de vouloir continuer ?",
      header: "Validation",
      icon: "pi pi-exclamation-triangle",
      accept:()=>{
        deleteUser().then((res)=>{
          console.log(res)
        }).catch((err)=>{
          console.log(err)
        })
      },
      reject,
    });
  };

  const confirm2 = (id) => {
    confirmDialog({
      message: "Voulez-vous supprimer cet enregistrement ?",
      header: "Confirmation de la suppression",
      icon: "pi pi-info-circle",
      acceptClassName: "p-button-danger",
      accept: () => {
        
        Users.deleteUser(id).then((data) => {
          toast.current.show({
            severity: "success",
            summary: "Suppression",
            detail: data.Message ,
            life: 3000,
          });
        });
      },
      reject,
    });
  };
  const onGlobalFilterChange2 = (e) => {
    const value = e.target.value;
    let _filters2 = { ...filters2 };
    _filters2["global"].value = value;

    setFilters2(_filters2);
    setGlobalFilterValue2(value);
  };

  const initFilters1 = () => {
    setFilters1({
      global: { value: null, matchMode: FilterMatchMode.CONTAINS },
      identifiant: {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.EQUALS }],
      },
      username: {
        operator: FilterOperator.AND,
        constraints: [{ value: null, matchMode: FilterMatchMode.CONTAINS }],
      },

      email: {
        operator: FilterOperator.OR,
        constraints: [{ value: null, matchMode: FilterMatchMode.CONTAINS }],
      },
      role: {
        operator: FilterOperator.OR,
        constraints: [{ value: null, matchMode: FilterMatchMode.EQUALS }],
      }
    });
    setGlobalFilterValue1("");
  };

  const renderHeader2 = () => {
    return (
      <div className="">
        <span className="p-input-icon-left">
          <i className="pi pi-search" />
          <InputText
            value={globalFilterValue2}
            onChange={onGlobalFilterChange2}
            placeholder="Recherche ..."
          />
        </span>
      </div>
    );
  };

  const idBodyTemplate = (rowData) => {
    return <span className="image-text">{rowData.id}</span>;
  };

  const usernameBodytemplate = (rowData) => {
    return <span>{rowData.username}</span>;
  };

  const emailBodyTemplate = (rowData) => {
    return (
      <span className={`customer-badge status-${rowData.email}`}>
        {rowData.email}
      </span>
    );
  };

  const roleBodyTemplate = (rowData) => {
    return (
      <span className={`customer-badge status-${rowData.role}`}>
        {rowData.role}
      </span>
    );
  };

  const statusItemTemplate = (option) => {
    return <span className={`customer-badge status-${option}`}>{option}</span>;
  };

  const exportCSV = () => {
    dt.current.exportCSV();
  };

  const statusRowFilterTemplate = (options) => {
    return (
      <Dropdown
        value={options.value}
        options={statuses}
        onChange={(e) => options.filterApplyCallback(e.value)}
        itemTemplate={statusItemTemplate}
        placeholder="Recherche par le role"
        className="p-column-filter"
        showClear
      />
    );
  };

  const actionBodyTemplate = (rowData) => {
    if(user.role=="admin"){
      return (
        <React.Fragment>
          <Button
            onClick={() => {
              confirm2(rowData.id);
            }}
            icon="pi pi-trash"
            className="p-button-rounded p-button-warning"
          />
        </React.Fragment>
      );
    }else{
      return (
        <></>
      )
    }

  };
  
  const leftToolbarTemplate = () => {
    if(user.role=="admin"){
      return (
        <React.Fragment>
          <Link to="/CreateUSer">
            <Button
              icon="pi pi-plus-circle"
              label="Créer"
              className="mr-2 p-button-info"
            ></Button>
          </Link>
          <Button
            onClick={confirm1}
            icon="pi pi-trash"
            label="Supprimer"
            className="mr-2 p-button-danger"
            disabled={!selectedSubmits || !selectedSubmits.length}
          ></Button>
        </React.Fragment>
      );
    }else{
      return(
        <></>
      )
    }

  };

  const rightToolbarTemplate = () => {
    return (
      <React.Fragment>
        <Button
          label="Export"
          icon="pi pi-upload"
          className="p-button-help"
          onClick={exportCSV}
        />
      </React.Fragment>
    );
  };

  const header2 = renderHeader2();
  const ArrayUsers=[]
  selectedSubmits.map((selecte, index)=>{
    ArrayUsers.push(selecte.id)
    console.log(ArrayUsers)
  })

  const deleteUser= async()=>{
    for (let i = 0; i < ArrayUsers.length; i++) {
      await fetch(urlBase + "/Users/" + ArrayUsers[i], {
        method: "DELETE"
      })
  }}

  return (
    <div>
      <Toast ref={toast} />
      <div className="datatable-filter-demo">
        <ConfirmDialog />
        <Toolbar
          className="mb-4"
          left={leftToolbarTemplate}
          right={rightToolbarTemplate}
        ></Toolbar>

        <DataTable
          selection={selectedSubmits}
          onSelectionChange={(e) => setSelectedSubmits(e.value)}
          dataKey="id"
          ref={dt}
          value={users}
          paginator
          className="p-datatable-customers"
          rows={5}
          filters={filters2}
          filterDisplay="row"
          loading={loading2}
          responsiveLayout="scroll"
          globalFilterFields={[
            "identifiant",
            "username",
            "email",
            "role",
          ]}
          header={header2}
          emptyMessage="Aucun utilisateur"
        >
          <Column
            selectionMode="multiple"
            headerStyle={{ width: "3em" }}
          ></Column>

          <Column
            field="identifiant"
            header="#"
            filterField="identifiant"
            filterPlaceholder="Recherche par identifiant"
            style={{ minWidth: "12rem" }}
            body={idBodyTemplate}
            filter
          />
          <Column
            filed="username"
            header="Nom d'utilisateur"
            filterField="username"
            style={{ minWidth: "12rem" }}
            body={usernameBodytemplate}
            filter
            filterPlaceholder="Recherche par le nom d'utilisateur"
          />

          <Column
            field="email"
            header="Email"
            showFilterMenu={false}
            filterMenuStyle={{ width: "14rem" }}
            style={{ minWidth: "12rem" }}
            body={emailBodyTemplate}
            filter
            filterPlaceholder="Recherche par le mail"
          />
          <Column
            field="role"
            header="Role"
            filterField="role"
            showFilterMenu={false}
            filterMenuStyle={{ width: "14rem" }}
            style={{ minWidth: "14rem" }}
            filter
            filterElement={statusRowFilterTemplate}
            body={roleBodyTemplate}
          />
          <Column
            body={actionBodyTemplate}
            exportable={false}
            style={{ minWidth: "8rem" }}
          ></Column>
        </DataTable>
      </div>
    </div>
  );
};
