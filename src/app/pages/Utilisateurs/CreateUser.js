import React, { useState, useEffect } from "react";
import { Button, Form } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { urlBase } from "../../consts";
import swal from "sweetalert";
import { validate } from "react-email-validator";
import { useSelector } from "react-redux";
import { Service } from "../Service";

export const CreateUser = () => {
  const { user } = useSelector((state) => state.auth);
  const [roles, setRole] = useState([]);
  const [name, setName] = useState();
  const [email, SetEmail] = useState();
  const [password, setPassword] = useState();
  const [idRole, setIdRole] = useState();
  const history = useHistory();
  const UserService = new Service();

  const handleOnChange = (e) => {
    setIdRole(e.target.value);
  };
  const handleName = (e) => {
    setName(e.target.value);
  };
  const handleEmail = (e) => {
    SetEmail(e.target.value);
  };

  const handlePassword = (e) => {
    console.log(e.target.value);
    setPassword(e.target.value);
  };

  const addUser = async () => {
    if (!email || !name || !password || !idRole) {
      swal("Tous les champs sont obligatoires", "!!!", "error");
    } else {
      if (typeof name != "string") {
        swal(
          "le nom d'utilisateur doit etre une chaine de caractere",
          "",
          "error"
        );
      }
      if (validate(email) != true) {
        swal("le mail n'est pas valide", "", "error");
      }
      if (email && name && password && idRole) {
        if (typeof name == "string" && validate(email) == true) {
          if (user.role != "admin") {
            swal("Action reservée aux administrateurs", "", "error");
            navigate.push("/dashboard");
          } else {
            UserService.addUser({
              name: name,
              email: email,
              password: password,
              role: idRole,
            })
              .then((response) => {
                if (response) {
                  swal("Enregistrement effectué", "", "success");
                  history.push("/list-users");
                } else {
                  swal("Erreur d'enregistrement", "", "error");
                }
              })
              .catch((err) => console.log(err));
          }
        }
      }
    }
  };

  // if(!email || !roles || name || password || idRole){
  //   swal("Tous les champs sont obligatoires","", "error")
  // }else{
  //   if(typeof(name)!="string"){
  //     swal("le nom d'utilisateur doit être une chaine de carateres")
  //   }
  // }

  // console.log("TEST")

  //   await fetch(urlBase + "/register", {
  //     method: "POST",
  //     headers: {
  //       Accept: "application/json",
  //       "Content-Type": "application/json",
  //     },
  //     body: JSON.stringify({
  //       name: name,
  //       email: email,
  //       password:password,
  //       role: idRole
  //     }),
  //   })
  //     .then((response) => {
  //       if (response) {
  //         swal("Utilisateur enregistré", "!!!", "success");
  //         setTimeout(navigate.push("/list-users"),10000)

  //       } else {
  //         swal("Erreur d'enregistrement", "!!!", "error");
  //       }
  //     })
  //     .catch((err) => console.log(err));
  // }
  const navigate = useHistory();

  const getRoles = async () => {
    const allRoles = await fetch(urlBase + "/roles").then((res) => res.json());
    console.log(allRoles);
    setRole(allRoles);
    console.log(roles);
  };

  useEffect(() => {
    getRoles();
    console.log("TEST");
  }, []);

  return (
    <>
      <div className="card card-custom card-sticky" id="kt_page_sticky_card">
        <div className="card-header">
          <div className="card-title">
            <h3 className="card-label">
              Creation d'un utilisateur<i className="mr-2"></i>
            </h3>
          </div>
        </div>
        <div className="card-body">
          <Form>
            <div className="row">
              <div className="col-xl-2"></div>
              <div className="col-xl-8">
                <div className="my-5">
                  <Form.Group
                    className="form-group row"
                    controlId="formBasicEmail"
                  >
                    <label className="col-3">Nom et prénoms</label>
                    <div className="col-9">
                      <Form.Control
                        type="text"
                        name="name"
                        className="form-control form-control-solid"
                        placeholder="John Doe"
                        onChange={handleName}
                        required="required"
                      />
                    </div>
                  </Form.Group>
                  <Form.Group
                    className="form-group row"
                    controlId="formBasicEmail"
                  >
                    <label className="col-3">Email</label>
                    <div className="col-9">
                      <Form.Control
                        type="email"
                        name="name"
                        className="form-control form-control-solid"
                        onChange={handleEmail}
                        required="required"
                      />
                    </div>
                  </Form.Group>
                  <Form.Group
                    className="form-group row"
                    controlId="formBasicEmail"
                  >
                    <label className="col-3">Mot de passe</label>
                    <div className="col-9">
                      <Form.Control
                        type="password"
                        name="password"
                        className="form-control form-control-solid"
                        onChange={handlePassword}
                        required="required"
                      />
                    </div>
                  </Form.Group>
                  <div className="form-group row">
                    <label className="col-3">choix du rôle utilisateur</label>
                    <div className="col-9">
                      <Form.Control
                        as="select"
                        onChange={handleOnChange}
                        className="form-control form-control-solid"
                      >
                        <option>+++++++Liste des rôles+++++++</option>
                        {roles.map(({ role, id }) => (
                          <option key={id} value={role}>
                            {role}
                          </option>
                        ))}
                      </Form.Control>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xl-2"></div>
            </div>
            <div className="card-footer">
              <div class="btn-toolbar">
                <Button
                  style={{ backgroundColor: "#F97A07" }}
                  className="btn btn-secondary"
                  onClick={addUser}
                >
                  Enregistrer
                </Button>
                <span style={{ color: "white" }}>rr</span>
                <button
                  className="btn btn-secondary"
                  onClick={() => navigate.push("/")}
                >
                  Annuler
                </button>
              </div>
            </div>
          </Form>
        </div>
      </div>
    </>
  );
};
