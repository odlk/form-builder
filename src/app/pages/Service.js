import { urlBase } from "../consts";
import { useSelector } from "react-redux";

export class Service {
  authToken = useSelector((state) => state.auth.authToken);

  /////////////////////Formation/////////////////////////

  async getFormation() {
    const res = await fetch(`${urlBase}/formations`, {
      headers: {
        Authorization: `Bearer ${this.authToken}`,
      },
    });
    const data = await res.json();
    return data;
  }

  async getFormationById(id) {
    const res = await fetch(`${urlBase}/formations/${id}`, {
      headers: {
        Authorization: `Bearer ${this.authToken}`,
      },
    });
    const data = await res.json();
    return data;
  }

  async addFormation(data) {
    const response = await fetch(urlBase + "/formations", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${this.authToken}`,
      },
      body: JSON.stringify(data),
    }).catch((err) => console.log(err));
    return response.json();
  }

  async editFormation(id, data) {
    const response = await fetch(`${urlBase}/formations/${id}`, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${this.authToken}`,
      },
      body: JSON.stringify(data),
    }).catch((err) => console.log(err));
    return response.json();
  }

  //////////////////////////////////End Formation//////////////////////////////

  ////////////////////////////////////////Formulaire////////////////////////////
  async getFormulaire() {
    const res = await fetch(`${urlBase}/Formulaires`, {
      headers: {
        Authorization: `Bearer ${this.authToken}`,
      },
    });
    const data = await res.json();
    return data;
  }

  async getFormulaireById(id) {
    const res = await fetch(`${urlBase}/Formulaires/${id}`, {
      headers: {
        Authorization: `Bearer ${this.authToken}`,
      },
    });
    const data = await res.json();
    return data;
  }

  async addFormulaire(data) {
    const response = await fetch(urlBase + "/Formulaires", {
      method: "POST",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${this.authToken}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    }).catch((err) => console.log(err));
    return response.json();
  }

  async editFormulaire(data, id) {
    const response = await fetch(urlBase + "/Formulaires/" + id, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${this.authToken}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    }).catch((err) => console.log(err));
    return response.json();
  }

  async deleteFormulaire(id) {
    const res = await fetch(`${urlBase}/Formulaires/${id}`, {
      method: "DELETE",
    });
    const data = await res.json();
    return data;
  }

  //////////////////////////////////End Formulaire///////////////////////////

  /////////////////////////////////////////Users/////////////////////////////////
  async getUsers() {
    const res = await fetch(`${urlBase}/Users`, {
      headers: {
        Authorization: `Bearer ${this.authToken}`,
      },
    });
    const data = await res.json();
    return data;
  }

  async getUserById(id) {
    const res = await fetch(`${urlBase}/Users/${id}`, {
      headers: {
        Authorization: `Bearer ${this.authToken}`,
      },
    });
    const data = await res.json();
    return data;
  }

  async addUser(data) {
    const response = await fetch(urlBase + "/register", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${this.authToken}`,
      },
      body: JSON.stringify(data),
    });
    return response.json();
  }

  async EditUser(id, data) {
    const response = await fetch(`${urlBase}/Users/${id}`, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${this.authToken}`,
      },
      body: JSON.stringify(data),
    });
    return response.json();
  }

  async deleteUser(id) {
    const res = await fetch(`${urlBase}/Users/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${this.authToken}`,
      },
    });
    const data = await res.json();
    return data;
  }

  //////////////////////////////////End User//////////////////////

  ////////////////////Roles///////////////////////////////////

  async getRoles() {
    const allRoles = await fetch(urlBase + "/roles", {
      headers: {
        Authorization: `Bearer ${this.authToken}`,
      },
    });
    const data = await allRoles.json();
    return data;
  }
  /////////////////////////End Roles////////////////////////////

  ////////////////////////////Soumission//////////////////////////
  async getSubmit() {
    const res = await fetch(`${urlBase}/SoumettreFormulaire`, {
      headers: {
        Authorization: `Bearer ${this.authToken}`,
      },
    });
    const data = await res.json();
    return data;
  }

  async getSubmitById(id) {
    const res = await fetch(`${urlBase}/SoumettreFormulaire/${id}`, {
      headers: {
        Authorization: `Bearer ${this.authToken}`,
      },
    });
    const data = await res.json();
    return data;
  }

  async soumission(data) {
    await fetch(urlBase + "/SoumettreFormulaire/", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${this.authToken}`,
      },
      body: JSON.stringify(data),
    });
  }

  async Validation(data, id) {
    await fetch(urlBase + "/Validation/" + id, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${this.authToken}`,
      },
      body: JSON.stringify(data),
    }).catch((err) => console.log(err));
  }

  async deleteFormation(id) {
    const res = await fetch(`${urlBase}/formations/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${this.authToken}`,
      },
    });
    const data = await res.json();
    return data;
  }

  ////////////////////////////Recherche//////////////////////////////////
  async Search(param) {
    const res = await fetch(`${urlBase}/Search/${param}`, {
      headers: {
        Authorization: `Bearer ${this.authToken}`,
      },
    });
    const data = await res.json();
    return data;
  }
  ////////////////////////////End Recherche/////////////////////////////

  ///////////////////////////////Counter/////////////////////////
  async Counter(param) {
    const res = await fetch(`${urlBase}/Counter`, {
      headers: {
        Authorization: `Bearer ${this.authToken}`,
      },
    });
    const data = await res.json();
    return data;
  }
}
