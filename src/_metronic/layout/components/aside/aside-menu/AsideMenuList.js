/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import { useLocation } from "react-router";
import { NavLink } from "react-router-dom";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl, checkIsActive } from "../../../../_helpers";
import { useSelector } from "react-redux";

export function AsideMenuList({ layoutProps }) {
  const location = useLocation();
  const { user } = useSelector((state) => state.auth);
  const getMenuItemActive = (url) => {
    return checkIsActive(location, url)
      ? " menu-item-active menu-item-open "
      : "";
  };

  return (
    <>
      {/* begin::Menu Nav */}
      <ul className={`menu-nav ${layoutProps.ulClasses}`}>
        {/*begin::1 Level*/}
        <li
          className={`menu-item ${getMenuItemActive("/dashboard")}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/dashboard">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")} />
            </span>
            <span className="menu-text">Tableau de bord</span>
          </NavLink>
        </li>

        <li className="menu-section ">
          <h4 className="menu-text">Menu</h4>
          <i className="menu-icon flaticon-more-v2"></i>
        </li>

        <li
          className={`menu-item menu-item-submenu ${getMenuItemActive(
            "/react-bootstrap"
          )}`}
          aria-haspopup="true"
          data-menu-toggle="hover"
        >
          <NavLink className="menu-link menu-toggle" to="/react-bootstrap">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Shopping/Box2.svg")} />
            </span>
            <span className="menu-text">Utilisateurs</span>
            <i className="menu-arrow" />
          </NavLink>
          <div className="menu-submenu ">
            <ul className="menu-subnav">
              <ul className="menu-subnav">
                <li
                  className="menu-item  menu-item-parent"
                  aria-haspopup="true"
                >
                  <span className="menu-link">
                    <span className="menu-text">Utilisateur</span>
                  </span>
                </li>

                {/*begin::2 Level*/}
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/CreateFormations"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink className="menu-link" to="/CreateUser">
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Creation d'un utilisateur</span>
                  </NavLink>
                </li>
                {/*end::2 Level*/}

                {/*begin::2 Level*/}
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/list-formations"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink className="menu-link" to="/list-users">
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">liste des utilisateurs</span>
                  </NavLink>
                </li>
              </ul>
            </ul>
          </div>
        </li>

        <li
          className={`menu-item menu-item-submenu ${getMenuItemActive(
            "/react-bootstrap"
          )}`}
          aria-haspopup="true"
          data-menu-toggle="hover"
        >
          <NavLink className="menu-link menu-toggle" to="/react-bootstrap">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Shopping/Box2.svg")} />
            </span>
            <span className="menu-text">Formations</span>
            <i className="menu-arrow" />
          </NavLink>
          <div className="menu-submenu ">
            <ul className="menu-subnav">
              <ul className="menu-subnav">
                <li
                  className="menu-item  menu-item-parent"
                  aria-haspopup="true"
                >
                  <span className="menu-link">
                    <span className="menu-text">Formation</span>
                  </span>
                </li>

                {/*begin::2 Level*/}
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/CreateFormations"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink className="menu-link" to="/CreateFormations">
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Creation de Formation</span>
                  </NavLink>
                </li>
                {/*end::2 Level*/}

                {/*begin::2 Level*/}
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/list-formations"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink className="menu-link" to="/list-formations">
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Liste des formations</span>
                  </NavLink>
                </li>
              </ul>
            </ul>
          </div>
        </li>

        <li
          className={`menu-item menu-item-submenu ${getMenuItemActive(
            "/react-bootstrap"
          )}`}
          aria-haspopup="true"
          data-menu-toggle="hover"
        >
          <NavLink className="menu-link menu-toggle" to="/react-bootstrap">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Shopping/Box2.svg")} />
            </span>
            <span className="menu-text">Formulaire</span>
            <i className="menu-arrow" />
          </NavLink>
          <div className="menu-submenu ">
            <ul className="menu-subnav">
              <ul className="menu-subnav">
                <li
                  className="menu-item  menu-item-parent"
                  aria-haspopup="true"
                >
                  <span className="menu-link">
                    <span className="menu-text">Formulaire</span>
                  </span>
                </li>

                {/*begin::2 Level*/}
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/CreateFormulaire"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink className="menu-link" to="/CreateFormulaire">
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Creation de Formulaire</span>
                  </NavLink>
                </li>
                {/*end::2 Level*/}

                {/*begin::2 Level*/}
                <li
                  className={`menu-item ${getMenuItemActive(
                    "/list-formulaires"
                  )}`}
                  aria-haspopup="true"
                >
                  <NavLink className="menu-link" to="/list-formulaires">
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Liste des formulaires</span>
                  </NavLink>
                </li>
              </ul>
            </ul>
          </div>
        </li>

        <li
          className={`menu-item menu-item-submenu ${getMenuItemActive(
            "/react-bootstrap"
          )}`}
          aria-haspopup="true"
          data-menu-toggle="hover"
        >
          <NavLink className="menu-link menu-toggle" to="/react-bootstrap">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Shopping/Box2.svg")} />
            </span>
            <span className="menu-text">Soumissions</span>
            <i className="menu-arrow" />
          </NavLink>
          <div className="menu-submenu ">
            <ul className="menu-subnav">
              <ul className="menu-subnav">
                <li
                  className="menu-item  menu-item-parent"
                  aria-haspopup="true"
                >
                  <span className="menu-link">
                    <span className="menu-text">Soumission</span>
                  </span>
                </li>
                {/*end::2 Level*/}

                {/*begin::2 Level*/}
                <li
                  className={`menu-item ${getMenuItemActive("/list-form")}`}
                  aria-haspopup="true"
                >
                  <NavLink className="menu-link" to="/list-submits">
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">Liste de soumissions</span>
                  </NavLink>
                </li>
                <li
                  className={`menu-item ${getMenuItemActive("/list-form")}`}
                  aria-haspopup="true"
                >
                  <NavLink className="menu-link" to="/extract">
                    <i className="menu-bullet menu-bullet-dot">
                      <span />
                    </i>
                    <span className="menu-text">
                      Faire mes extractions de canditatures
                    </span>
                  </NavLink>
                </li>
                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}

                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}

                {/*begin::2 Level*/}
                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}

                {/*begin::2 Level*/}

                {/*end::2 Level*/}
              </ul>
            </ul>
          </div>
        </li>
      </ul>
    </>
  );
}
