import React, { useState, useEffect } from "react";
import { urlBase } from "../../../app/consts";
import { Service } from "../../../app/pages/Service";

export function Demo1Dashboard() {
  const [stats, setStats] = useState([]);
  const counterService = new Service();
  const statDashboard = async () => {
    counterService.Counter().then((data) => {
      setStats(data);
    });
  };

  useEffect(() => {
    statDashboard();
    console.log(stats.Utilisateurs);
  }, []);

  return (
    <>
      <div className="row">
        <div className="col-lg-12 grid">
          <div className="col-lg-6 col-xxl-3 mb-3 mt-6">
            <div className="surface-0 shadow-2 p-3 border-1 border-50 border-round">
              <div className="flex justify-content-between mb-3">
                <div>
                  <span className="block text-500 font-medium mb-3">
                    Utilisateurs
                  </span>
                  <div className="text-900 font-medium text-xl">
                    {stats.Utilisateurs}
                  </div>
                </div>
                <div
                  className="flex align-items-center justify-content-center bg-blue-100 border-round"
                  style={{ width: "2.5rem", height: "2.5rem" }}
                >
                  <i className="pi pi-users text-blue-500 text-xl"></i>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-xxl-3 mb-3 mt-6">
            <div className="surface-0 shadow-2 p-3 border-1 border-50 border-round">
              <div className="flex justify-content-between mb-3">
                <div>
                  <span className="block text-500 font-medium mb-3">
                    Formations
                  </span>
                  <div className="text-900 font-medium text-xl">
                    {stats.Formations}
                  </div>
                </div>
                <div
                  className="flex align-items-center justify-content-center bg-orange-100 border-round"
                  style={{ width: "2.5rem", height: "2.5rem" }}
                >
                  <i className="pi pi-shopping-bag text-orange-500 text-xl"></i>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-xxl-3 mb-3 mt-6">
            <div className="surface-0 shadow-2 p-3 border-1 border-50 border-round">
              <div className="flex justify-content-between mb-3">
                <div>
                  <span className="block text-500 font-medium mb-3">
                    Formulaires
                  </span>
                  <div className="text-900 font-medium text-xl">
                    {stats.Formulaires}
                  </div>
                </div>
                <div
                  className="flex align-items-center justify-content-center bg-cyan-100 border-round"
                  style={{ width: "2.5rem", height: "2.5rem" }}
                >
                  <i
                    className="pi
pi-file text-cyan-500 text-xl"
                  ></i>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-xxl-3 mb-3 mt-6">
            <div className="surface-0 shadow-2 p-3 border-1 border-50 border-round">
              <div className="flex justify-content-between mb-3">
                <div>
                  <span className="block text-500 font-medium mb-3">
                    Soumissions
                  </span>
                  <div className="text-900 font-medium text-xl">
                    {stats.Soumissions}
                  </div>
                </div>
                <div
                  className="flex align-items-center justify-content-center bg-purple-100 border-round"
                  style={{ width: "2.5rem", height: "2.5rem" }}
                >
                  <i className="pi pi-download text-purple-500 text-xl"></i>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* <div className="col-lg-6 col-xxl-4">
          <MixedWidget1 className="card-stretch gutter-b" />
        </div>
        <div className="col-lg-6 col-xxl-4">
          <ListsWidget9 className="card-stretch gutter-b" />
        </div>

        <div className="col-lg-6 col-xxl-4">
          <StatsWidget11 className="card-stretch card-stretch-half gutter-b" />
          <StatsWidget12 className="card-stretch card-stretch-half gutter-b" />
        </div>

        <div className="col-lg-6 col-xxl-4 order-1 order-xxl-1">
          <ListsWidget1 className="card-stretch gutter-b" />
        </div>
        <div className="col-xxl-8 order-2 order-xxl-1">
          <AdvanceTablesWidget2 className="card-stretch gutter-b" />
        </div>
        <div className="col-lg-6 col-xxl-4 order-1 order-xxl-2">
          <ListsWidget3 className="card-stretch gutter-b" />
        </div>
        <div className="col-lg-6 col-xxl-4 order-1 order-xxl-2">
          <ListsWidget4 className="card-stretch gutter-b" />
        </div>
        <div className="col-lg-12 col-xxl-4 order-1 order-xxl-2">
          <ListsWidget8 className="card-stretch gutter-b" />
        </div> */}
      </div>
    </>
  );
}
